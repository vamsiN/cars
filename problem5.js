// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require('./inventory')
const problem4 = require('./problem4')

const car_year_array = problem4(inventory)

let cars_older_than_2000_array = []

const problem5 =(inventory,car_years_array) =>{
    for (let car of inventory){
        if (car.car_year < 2000){
            cars_older_than_2000_array.push(car)
        }
    }
    return cars_older_than_2000_array
} 
    

module.exports = problem5