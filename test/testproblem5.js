const inventory = require('../inventory')
const problem5 = require('../problem5')

const result5 = problem5(inventory)
console.log(result5)
console.log(`Number of cars older than 2000 is ${result5.length}`)      //works only for problem5
