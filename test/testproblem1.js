const inventory = require('../inventory')
const problem1 = require('../problem1')


const result1 = problem1()
console.log(result1)

const result2 = problem1([]) 
console.log(result2)

const result3 = problem1(inventory)
console.log(result3)

//Your code threw an error when a search id is passed to problem1. Ensure that you are doing the appropriate data checks before working on the data passed.
const result4 = problem1(33) 
console.log(result4)

// // //An array or object should be returned by problem1 when there is a match to the id. Make sure that you read and understand the problem before starting to write the code. Otherwise your code is not reusable.
const result5 = problem1(inventory, 1) 
console.log(result5)

const result6 = problem1(33,inventory)
console.log(result6)

const result7 = problem1(new String("hello"), 1)
console.log(result7)

const result8 = problem1({name: "Test"}, 33)
console.log(result8)

const result9 = problem1(inventory, [])
console.log(result9)

const result10 = problem1([{"id":0,"car_make":"Dodge","car_model":"Magnum","car_year":2008}], 0)
console.log(result10)

const result11 = problem1(1)
console.log(result11)

const result12 = problem1({id: 33, name: "Test", length: 10}, 33)
console.log(result12)









