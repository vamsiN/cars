// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const bmw_and_audi_cars_array = []
const problem6 = (inventory) =>{
    for(let car of inventory){
        if (car.car_make == "BMW" || car.car_make == "Audi"){
            bmw_and_audi_cars_array.push(car)
        }
    }
    return bmw_and_audi_cars_array
}
module.exports = problem6




