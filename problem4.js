// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

let cars_year_array =[]

const problem4= (inventory) => {

    for(let i = 0; i<inventory.length; i++){
        cars_year_array.push(inventory[i].car_year)
    }

    return cars_year_array
}

module.exports = problem4