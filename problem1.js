
const inventory = require("./inventory")

function search_id(inventory,n){
    
    let is_id_key_in_object = "id" in inventory[0]

    if(is_id_key_in_object == false){
        return []
    }else{
        let car_found = false
        let result_car = null
        for( let car of inventory){
            if (car.id == n){
                car_found = true
                result_car = car
            }
        }

        if (car_found == true){
            return result_car
        }else{
            return []
        }
    }    
}

function problem1 (arg0,arg1){

    if (arguments.length == 0){return []}
    if (arguments.length ==1){return []}

    if(arguments.length ==2){
        if (arg0 instanceof Array && typeof(arg1) == 'number'){
            return search_id(arg0,arg1)
        } 
        else if (typeof(arg0) == 'number' && arg1 instanceof Array){
            return search_id(arg1,arg0)
        }
        else{               // if arg0 is string or object 
            return []
        }


    }
}
    
module.exports = problem1